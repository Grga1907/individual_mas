import numpy as np

class grid:
    def __init__(self, walls):
        self.world = None
        self.end_state = {}
        self.walls = walls
    def create_world(self, size):
        self.world = np.zeros([size, size])

    def add_walls(self, walls):
        for wall in walls:
            self.world[wall[0]][wall[1]] = None

    def add_absorbing_states(self, states):
        for state in states:
            self.world[state[0]][state[1]] = state[2]
            self.end_state[(state[0], state[1])] = state[2]
