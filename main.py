from grid import grid
from agent import agent
import learningmethods
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

walls = {(1, 2): 0, (1, 3): 0, (1, 4): 0, (1, 5): 0, (1, 6): 0,
         (2, 6): 0,
         (3, 6): 0,
         (4, 6): 0,
         (5, 6): 0,
         (7, 1): 0, (7, 2): 0, (7, 3): 0, (7, 4): 0}
absorbing_states = [[6, 5, -50], [8, 8, 50]]


#Policy world for Monte carlo policy with equiprobable actions
policy_world = grid(walls)
policy_world.create_world(9)
policy_world.add_walls(walls)
policy_world.add_absorbing_states(absorbing_states)

#Grid world for SARSA and QLEARNING
gwq = grid(walls)
gwq.create_world(9)
gwq.add_walls(walls)
gwq.add_absorbing_states(absorbing_states)

gws = grid(walls)
gws.create_world(9)
gws.add_walls(walls)
gws.add_absorbing_states(absorbing_states)

#Starting position and exploration rate for the Agent
starting_position = (0, 0)
exploration_rate = 0.05

player = agent(starting_position, exploration_rate)


#Monte carlo policy & heatmap
player.monte_carlo_policy(policy_world, 1000)
ax = sns.heatmap(policy_world.world, linewidth=0.5, cmap="vlag", annot=True, vmin=-25, vmax=25)
ax.set_facecolor('xkcd:green')
plt.show()

#Q-Learning or Sarsa method with policy in mind or without policy in mind
player = agent(starting_position, exploration_rate)
reward1 = player.play(gwq, 1000, learningmethods.Q_LEARNING)
# ax = sns.heatmap(gwq.world, linewidth=0.5, cmap="vlag", annot=True, vmax=20, vmin=-20)
# ax.set_facecolor('xkcd:green')
# plt.show()

player = agent(starting_position, exploration_rate)
reward2 = player.play(gws, 1000, learningmethods.SARSA)
# ax = sns.heatmap(gws.world, linewidth=0.5, cmap="vlag", annot=True, vmax=0)
# ax.set_facecolor('xkcd:green')
# plt.show()

# avg1 = []
# avg2 = []
# for i in range(len(reward1)-1):
#     avg1.append(np.average(reward1[:i+1]))
#     avg2.append(np.average(reward2[:i+1]))

plt.xlabel("Simulation number")
plt.ylabel("Value acquired")
plt.plot(reward2, label="Q-Learning")
plt.plot(reward1, label="SARSA")
plt.legend()
plt.show()
