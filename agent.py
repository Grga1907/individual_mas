import numpy as np
import random
from tqdm import tqdm
import learningmethods

class agent:
    def __init__(self, starting_position, exploration_rate):
        self.position = starting_position
        self.states = [starting_position]
        self.actions = np.array(["north", "west", "south", "east"])
        self.exploration_rate = exploration_rate

    def move(self, action, world):
        if action == "north":
            nxtPos = (self.position[0] - 1, self.position[1])
        elif action == "south":
            nxtPos = (self.position[0] + 1, self.position[1])
        elif action == "west":
            nxtPos = (self.position[0], self.position[1] - 1)
        elif action == "east":
            nxtPos = (self.position[0], self.position[1] + 1)
        else:
            return self.position

        if nxtPos[0] >= 0 and nxtPos[0] <= 8:
            if nxtPos[1] >= 0 and nxtPos[1] <= 8:
                if world.walls.get((nxtPos[0], nxtPos[1])) is None:
                    return nxtPos

        return self.position

    def play(self, world, episodes=50, learn_method=learningmethods.SARSA, learning_rate=0.4, decay_factor=0.999):
        round = 0
        returns = {(i, j): list() for i in range(9) for j in range(9)}
        episode_reward = 0
        total_reward = []
        max_steps = 2500
        while round < episodes:
            i = 0
            while i < max_steps:
                if self.isStateEnd(world):
                    # Give rewards
                    reward = world.end_state.get(self.position)
                    total_reward.append(reward + episode_reward)
                    self.calculate_reward(world, reward, learning_rate, decay_factor, True)
                    round += 1
                    i = 0
                    print(round)
                    self.position = (0, 0)
                    episode_reward = 0
                    break
                else:
                    if learn_method == learningmethods.SARSA:
                        action = self.epsilon_greedy_action(world)
                    else:
                        action = self.epsilon_greedy_action(world, True)
                    self.states.append(self.move(action, world))
                    # print(f"current position {self.position} action {action}")
                    if world.end_state.get(self.states[-1]) is None:
                        reward = -1
                        episode_reward += reward
                        self.calculate_reward(world, reward, learning_rate, decay_factor, False)

                    self.position = self.states[-1]
                    i+= 1
        return total_reward

    def isStateEnd(self, world):
        if world.end_state.get(self.position) is None:
            return False
        return True

    def epsilon_greedy_action(self, world, train=False):
        reward = {}

        #If train = True, then we should skip this
        if train or np.random.uniform(0, 1) > self.exploration_rate:
            # greedy action
            for action in self.actions:
                # if the action is deterministic
                next_reward = world.world[self.move(action, world)]
                reward[self.move(action, world)] = next_reward
            best_reward = self.pick_best(reward)
            if self.position[0] - best_reward[0] != 0:
                if self.position[0] - best_reward[0] == 1:
                    return "north"
                else:
                    return "south"
            elif self.position[1] - best_reward[1] != 0:
                if self.position[1] - best_reward[1] == 1:
                    return "west"
                else:
                    return "east"
            else:
                return "same"
        else:
            next_action = np.random.choice(self.actions)
        return next_action

    def pick_best(self, reward):
        mx = None
        max_vals = []
        for val in reward:
            if mx is None:
                mx = reward.get(val)
            if reward.get(val) == mx:
                max_vals.append(val)
            elif reward.get(val) > mx:
                max_vals = [val]
                mx = reward.get(val)
        if len(max_vals) > 1:
            return max_vals[random.randint(0, len(max_vals)-1)]
        return max_vals[0]

    def calculate_reward(self, world, reward, learning_rate, decay_factor, done):
        ns = self.states[-1]
        next_state_reward = world.world[ns[0]][ns[1]]
        if done:
            world.world[self.position[0]][self.position[1]] += learning_rate * (reward - world.world[self.position[0]][self.position[1]])
        else:
            world.world[self.position[0]][self.position[1]] += learning_rate * (reward + (decay_factor * next_state_reward) - world.world[self.position[0]][self.position[1]])


    #Generate monte carlo simulation with equiprobable moves
    def generateEpisode(self, world, starting_position=None):
        self.states = []
        if starting_position is None:
            while True:
                state = np.random.randint(9, size=2)
                self.position = (state[0], state[1])
                if world.walls.get(self.position) is None:
                    break
        else:
            self.position = starting_position
            self.states.append(self.position)
        while True:
            if world.end_state.get(self.position):
                return self.states
            prev = self.position
            self.position = self.move(np.random.choice(self.actions), world)
            if world.end_state.get(self.position):
                self.states.append([prev, world.end_state.get(self.position), self.position])
            else:
                self.states.append([prev, -1, self.position])

    #DO monte carlo
    def monte_carlo_policy(self, world, iterations=10000, starting_position=None):
        gamma = 0.9  # discounting rate
        returns = {(i, j): list() for i in range(9) for j in range(9)}
        for it in tqdm(range(iterations)):
            self.generateEpisode(world, starting_position)
            G = 0
            for i, step in enumerate(self.states[::-1]):
                G = gamma * G + step[1]
                if step[0] not in [x[0] for x in self.states[::-1][len(self.states) - i:]]:
                    idx = (step[0][0], step[0][1])
                    returns[idx].append(G)
                    newValue = np.average(returns[idx])
                    world.world[idx[0], idx[1]] = newValue
